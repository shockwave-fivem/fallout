local boardObjects = {}

--------------------------------------------------------------------------------

local GAME_STATE_CREATING = 'GAME_STATE_CREATING'
local GAME_STATE_SPAWNING = 'GAME_STATE_SPAWNING'
local GAME_STATE_PLAYING  = 'GAME_STATE_PLAYING'

local gameState = nil

--------------------------------------------------------------------------------

local PLAYER_STATE_CONNECTED = 'PLAYER_STATE_CONNECTED'
local PLAYER_STATE_WAITING   = 'PLAYER_STATE_WAITING'
local PLAYER_STATE_READY     = 'PLAYER_STATE_READY'
local PLAYER_STATE_ALIVE     = 'PLAYER_STATE_ALIVE'
local PLAYER_STATE_DEAD      = 'PLAYER_STATE_DEAD'

local players = setmetatable({}, {
  __len = function (xs)
    local count = 0
    for _ in pairs (xs) do
      count = count + 1
    end
    return count
  end
})

--------------------------------------------------------------------------------

RegisterNetEvent('fallout:onPlayerConnected')
AddEventHandler('fallout:onPlayerConnected', function ()
  players[source] = PLAYER_STATE_CONNECTED

  if #players == 1 then
    -- boardObjects = CreateGrid(10, 10)

    -- gameState = GAME_STATE_CREATING
    players[source] = PLAYER_STATE_WAITING
  end

  TriggerClientEvent('fallout:__createBoard', source, boardObjects)
end)

RegisterNetEvent('fallout:onPlayerReady')
AddEventHandler('fallout:onPlayerReady', function ()
  players[source] = PLAYER_STATE_READY
  TriggerClientEvent('fallout:__spectate', source)
end)

RegisterNetEvent('fallout:onPlayerSpawned')
AddEventHandler('fallout:onPlayerSpawned', function ()
  players[source] = PLAYER_STATE_ALIVE
end)

RegisterNetEvent('fallout:onPlayerDied')
AddEventHandler('fallout:onPlayerDied', function ()
  players[source] = PLAYER_STATE_DEAD
  TriggerClientEvent('fallout:__spectate', source)
end)

AddEventHandler('playerDropped', function ()
  players[source] = nil
end)

--------------------------------------------------------------------------------

Citizen.CreateThread(function ()
  local function GameLoop()
    if gameState == GAME_STATE_CREATING then
      print('do something while the game isvgvvvvvdgfdfgfd being created')
      -- if not table.includes(players, PLAYER_STATE_WAITING) then
      --   print('all players should now spawn simultaniously')
      --   for player in pairs(players) do
      --     if players[player] == PLAYER_STATE_SPECTATING then
      --       players[player] = PLAYER_STATE_SPAWNING
      --       TriggerClientEvent('fallout:__spawnPlayer', player)
      --     end
      --   end
      -- end
    elseif gameState == GAME_STATE_SPAWNING then
      print('do something while players spawn')
    elseif gameState == GAME_STATE_PLAYING then
      print('do something while players play')
    end
  end

  while true do
    Citizen.Wait(1000)
    GameLoop()
  end
end)
