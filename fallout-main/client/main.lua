local GetAngleBetween2DVectors = GetAngleBetween_2dVectors

--------------------------------------------------------------------------------

local camera = nil
local mapObjects = {}

--------------------------------------------------------------------------------

AddEventHandler('onClientResourceStart', function (resourceName)
  if resourceName == GetCurrentResourceName() then
    if GetIsLoadingScreenActive() then
      ShutdownLoadingScreen()
      DoScreenFadeIn(500)
    end

    TriggerServerEvent('fallout:onPlayerConnected')
  end
end)

AddEventHandler('onResourceStop', function (resourceName)
  if resourceName == GetCurrentResourceName() then
    for _, object in pairs(mapObjects) do
      DeleteObject(object)
    end
  end
end)

--------------------------------------------------------------------------------

RegisterNetEvent('fallout:__createBoard')
AddEventHandler('fallout:__createBoard', function (grid)
  local hash = LoadModel(BOARD_OBJECT_MODEL)
  local size = GetModelSize(BOARD_OBJECT_MODEL)

  for _, cell in ipairs(grid) do
    local offX = size.x * (cell.row - 1)
    local offY = size.z * (cell.col - 1)
    local off = vector3(offX, offY, 0.0)

    local pos = BOARD_OBJECT_POS + off
    local rot = BOARD_OBJECT_ROT

    local object = CreateRotatedObject(hash, pos, rot)

    table.insert(mapObjects, object)
  end

  TriggerServerEvent('fallout:onPlayerReady')
end)

RegisterNetEvent('fallout:__shakePiece')
AddEventHandler('fallout:__shakePiece', function (row, col)
end)

RegisterNetEvent('fallout:__dropPiece')
AddEventHandler('fallout:__dropPiece', function (row, col)
end)

RegisterNetEvent('fallout:__spectate')
AddEventHandler('fallout:__spectate', function ()
  if IsCamActive(camera) then
    print('camera already exists?')
  end

  camera = CreateCam('DEFAULT_SCRIPTED_CAMERA', true)

  -- TODO: Freeze player and shit
  SetEntityCoords(PlayerPedId(), 712.5, 3880.0, 220.0)

  SetCamCoord(camera, 712.5, 3890.0, 210.0)
  SetCamRot(camera, -25.0, 0.0, 0.0)
  SetCamFov(camera, 55.0)

  RenderScriptCams(true)
end)

RegisterNetEvent('fallout:__spawnPlayer')
AddEventHandler('fallout:__spawnPlayer', function ()
  if IsCamActive(camera) then
    DestroyCam(camera)
    RenderScriptCams(false)
  end

  local obj = math.random(#mapObjects)
  local pos = GetEntityCenter(mapObjects[obj])

  -- use GetAngleBetween2DVectors for heading

  SpawnPlayer('ig_bankman', pos.x, pos.y, pos.z, 0.0)
  SetPedDiesInstantlyInWater(PlayerPedId(), true)

  TriggerServerEvent('fallout:onPlayerSpawned')
end)

--------------------------------------------------------------------------------

Citizen.CreateThread(function ()
  local function HideHudLoop()
    if IsCamActive(camera) then
      HideHudAndRadarThisFrame()
    end
  end

  while true do
    Citizen.Wait(0)
    HideHudLoop()
  end
end)

--------------------------------------------------------------------------------

Citizen.CreateThread(function ()
  local function KillPlayerLoop()
    local playerPed = PlayerPedId()
    local pos = GetEntityCoords(playerPed)

    if not IsEntityDead(playerPed) and pos.z < 175.0 then
      KillPed(playerPed)
    end
  end

  while true do
    Citizen.Wait(100)
    KillPlayerLoop()
  end
end)
