function GetModelSize(hash)
  local min, max = GetModelDimensions(hash)

  local sizeX = math.abs(min.x) + math.abs(max.x)
  local sizeY = math.abs(min.y) + math.abs(max.y)
  local sizeZ = math.abs(min.z) + math.abs(max.z)

  return vector3(sizeX, sizeY, sizeZ)
end

function LoadModel(model)
  local hash = GetHashKey(model)

  repeat
    Citizen.Wait(0)
    RequestModel(hash)
  until HasModelLoaded(hash)

  return hash
end

function LoadCollisionAroundEntity(entity)
  local pos = GetEntityCoords(entity)

  repeat
    Citizen.Wait(0)
    RequestCollisionAtCoord(pos.x, pos.y, pos.z)
  until HasCollisionLoadedAroundEntity(entity)

  return true
end

function GetEntityCenter(entity)
  local model = GetEntityModel(entity)
  local pos = GetEntityCoords(entity)

  local min = GetModelDimensions(model)
  local hlf = GetModelSize(model) / 2

  local minPos = GetOffsetFromEntityInWorldCoords(entity, -min.x, -min.y, -min.z)
  local ctrPos = GetOffsetFromEntityInWorldCoords(entity,  hlf.x,  hlf.y,  hlf.z)

  return ctrPos
end

function CreateRotatedObject(hash, pos, rot)
  local object = CreateObjectNoOffset(hash, pos.x, pos.y, pos.z)
  SetEntityRotation(object, rot.x, rot.y, rot.z)
  return object
end

function SpawnPlayer(model, x, y, z, heading)
  local playerId = PlayerId()
  local playerPed = PlayerPedId()

  SetEntityCoords(playerPed, x, y, z)
  NetworkResurrectLocalPlayer(x, y, z, heading, true, true, false)

  ClearPedBloodDamage(playerPed)
  ClearPedTasksImmediately(playerPed)
  ClearPlayerWantedLevel(playerId)

  LoadCollisionAroundEntity(playerPed)

  local hash = LoadModel(model)
  SetPlayerModel(playerId, hash)
end

function KillPed(ped)
  return SetEntityHealth(ped, 0)
end
